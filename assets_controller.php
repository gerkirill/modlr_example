<?php
use Symfony\Component\HttpFoundation\Response;

$assetsController = $app['controllers_factory'];

$assetsController->get('/widgets_scripts', function () use ($app) {
	$widgetScripts = '';
	foreach(new \DirectoryIterator(__DIR__.'/views') as $fileInfo) {
		if ($fileInfo->isDir() && file_exists($fileInfo->getPathname().'/widget.js')) {
			$widgetScripts .= file_get_contents($fileInfo->getPathname().'/widget.js')."\n\n";
		}
	}
    return new Response($widgetScripts, 200, array('Content-type' => 'text/javascript'));
});

$assetsController->get('/widgets_css', function () use ($app) {
	// TODO: create style.less from template (widgets.less.twig ?) and compile it with lessc
	$widgetLess = '';
	foreach(new \DirectoryIterator(__DIR__.'/views') as $fileInfo) {
		if ($fileInfo->isDir() && file_exists($fileInfo->getPathname().'/widget.less')) {
			$widgetLess .= file_get_contents($fileInfo->getPathname().'/widget.less')."\n\n";
		}
	}
	file_put_contents(__DIR__.'/views/widgets.less', $widgetLess);
	$mainLessFile = __DIR__.'/views/style.less';
	$exitCode = 0;
	ob_start();
	system("lessc $mainLessFile 2>&1", $exitCode);
	$css = ob_get_clean();
    return new Response($css, $exitCode?500:200, array('Content-type' => 'text/css'));
});

return $assetsController;