<?php
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

require_once(__DIR__.'/bootstrap.php');

$app->get('/', function () use ($app) {
    return $app['twig']->render('index.twig');
});

$app->get('/form', function () use ($app) {
    return $app['twig']->render('todo_form/widget.twig');
});

$app->get('/list', function () use ($app) {
    return $app['twig']->render('todo_list/widget.twig', array(
    	'todos' => $app['storage']->getTodos()
    ));
});

$app->get('/delete/{id}', function ($id, Request $request) use ($app) {
	$app['storage']->removeTodo($id);
    return $app->redirect($request->getBaseUrl().'/list');
});

$app->post('/add', function(Request $request) use ($app) {
	$app['storage']->addTodo(
		array('title' => $request->get('todo'))
	);
	return $app->json(array('result'=>'ok'));
});

$app->run();