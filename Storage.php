<?php
class Storage {

	private $filePath;
	private $todos;

	public function __construct($path) {
		$this->filePath = $path;
		$this->todos = json_decode(file_get_contents($path));
	}

	public function getTodos() {
		return $this->todos;
	}

	public function addTodo($todo) {
		$this->todos[] = $todo;
		$this->flush();
	}

	public function removeTodo($index) {
		unset($this->todos[$index]);
    	$this->todos = array_values($this->todos);
    	$this->flush();
	}

	private function flush() {
		file_put_contents($this->filePath, json_encode($this->todos));
	}
}