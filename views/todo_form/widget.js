// todo form widget
window.modlr.defineModule('todo_form', function(module){
	var $ = module.$;

	function _addTodo(data, widget) {
		$.post(window.baseUrl+'/add', data, function(resp) {
			if (resp.result == 'ok') {
				widget.$.find('.data-todo-title').val('');
				module.trigger('todo_added', {});
			}
		});
	}

	return {
		init: function(widget) {
			widget.$.find('.data-todo-title').focus();
			widget.$.on('submit', '.data-todo-form', function(event) {
				var data = $(this).serialize();
				_addTodo(data, widget);
				return false;
			});
		}
	}
});