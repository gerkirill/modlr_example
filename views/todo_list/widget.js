// todo list widget
window.modlr.defineModule('todo_list', function(module){
	var $ = module.$;

	function _deleteTodo(widget, url) {
		$.get(url, function(widgetContent){
			var newWidget = module.replaceWidget(widget, widgetContent);
		});
	}

	function _reloadWidget(event, widget) {
		var url = window.baseUrl + '/list';
		$.get(url, function(widgetContent){
			var newWidget = module.replaceWidget(widget, widgetContent);
		});
	}

	return {
		init: function(widget) {
			// TODO: add sortable
			widget.$.on('click', '.data-delete-link', function(event) {
				_deleteTodo(widget, $(this).data('url'));
				return false;
			});
		},
		listeners: {
			'todo_added': _reloadWidget
		}
	}
});