// todo list widget
window.modlr.defineModule('notification', function(module){
	var $ = module.$;
	var self = {};

	self.notifyTodoAdded = function(event, widget) {
		widget.$.html('Todo saved');
		widget.$.show();
	}

	return {
		init: function(widget) {
			widget.$.on('click', function(){
				widget.$.html('');
				widget.$.hide();
				return false;
			})
		},
		listeners: {
			'todo_added': self.notifyTodoAdded
		}
	}
});