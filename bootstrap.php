<?php
error_reporting(E_ALL);
ini_set('display_errors', 'on');
require_once __DIR__.'/vendor/autoload.php';
$app = new Silex\Application();
$app['debug'] = true;
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/views'
));

call_user_func(function($app){
	$assetsController = require(__DIR__.'/assets_controller.php');
	$app->mount('/assets', $assetsController);
}, $app);

require(__DIR__.'/Storage.php');
$app['storage'] = $app->share(function () {
	return new Storage(__DIR__ . '/data.json');
});